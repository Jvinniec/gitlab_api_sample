# GitLab API Sample
This repository shows how to read and use a data file loaded from a GitLab based repository.

# API call breakdown
The API URL used to get a file from a GitLab repository is as follows:
```
https://gitlab.com/api/v4/projects/<PROJECT-ID>/repository/files/<PATH-TO-FILE>/raw?ref=<BRANCH-NAME>
```
The necessary components are:
* `PROJECT-ID`: The ID associated with the project. This is an integer that can be found on the repository's homepage in GitLab.
* `PATH-TO-FILE`: The path to the file inside the repository.
    * Replace '`/`' with `%2F`
* `BRANCH-NAME`: The name of the branch from which to pull the file. This allows loading different versions of the same file
